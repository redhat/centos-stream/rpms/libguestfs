From 61c266ec6c4190f9ad27413cc15461678e5979d6 Mon Sep 17 00:00:00 2001
From: "Richard W.M. Jones" <rjones@redhat.com>
Date: Mon, 29 Jul 2013 14:47:56 +0100
Subject: [PATCH] RHEL: Disable unsupported remote drive protocols
 (RHBZ#962113).

This disables support for unsupported remote drive protocols:

 * ftp
 * ftps
 * http
 * https
 * iscsi
 * ssh

Note 'nbd' is not disabled, and of course 'file' works.
---
 docs/guestfs-testing.pod               |  20 -----
 fish/guestfish.pod                     |  42 ++-------
 fish/test-add-uri.sh                   |  21 -----
 generator/actions_core.ml              |  34 +-------
 lib/drives.c                           | 113 -------------------------
 lib/guestfs.pod                        |  67 ---------------
 tests/disks/test-qemu-drive-libvirt.sh |   7 --
 tests/disks/test-qemu-drive.sh         |  40 ---------
 8 files changed, 8 insertions(+), 336 deletions(-)

diff --git a/docs/guestfs-testing.pod b/docs/guestfs-testing.pod
index 45ebe2941..a02f766c9 100644
--- a/docs/guestfs-testing.pod
+++ b/docs/guestfs-testing.pod
@@ -109,26 +109,6 @@ image.  To exit, type C<exit>.
 If you get an error, try enabling debugging (add C<-v> to the command
 line).  Also make sure that L<libguestfs-test-tool(1)> succeeds.
 
-=head2 Try to open a remote guest image with guestfish.
-
-You may also have to disable libvirt by setting this:
-
- export LIBGUESTFS_BACKEND=direct
-
-If you have a disk image available over HTTP/FTP, try to open it.
-
- guestfish --ro -i --format=raw -a http://www.example.com/disk.img
-
-For SSH you will need to make sure that ssh-agent is set up so you
-don't need a password to log in to the remote machine.  Then a command
-similar to this should work:
-
- guestfish --ro -i --format=raw \
-   -a ssh://remote.example.com/path/to/disk.img
-
-If you get an error, try enabling debugging (add C<-v> to the command
-line).  Also make sure that L<libguestfs-test-tool(1)> succeeds.
-
 =head2 Run virt-alignment-scan on all your guests.
 
 Run L<virt-alignment-scan(1)> on guests or disk images:
diff --git a/fish/guestfish.pod b/fish/guestfish.pod
index ac14ea5a4..8d4d488fc 100644
--- a/fish/guestfish.pod
+++ b/fish/guestfish.pod
@@ -131,9 +131,9 @@ To list what is available do:
 
 =head2 Remote drives
 
-Access a remote disk using ssh:
+Access a remote disk using NBD:
 
- guestfish -a ssh://example.com/path/to/disk.img
+ guestfish -a nbd://example.com
 
 =head2 Remote control
 
@@ -1129,12 +1129,12 @@ L<guestfs(3)/REMOTE STORAGE>>.
 On the command line, you can use the I<-a> option to add network
 block devices using a URI-style format, for example:
 
- guestfish -a ssh://root@example.com/disk.img
+ guestfish -a nbd://example.com
 
 URIs I<cannot> be used with the L</add> command.  The equivalent
 command using the API directly is:
 
- ><fs> add /disk.img protocol:ssh server:tcp:example.com username:root
+ ><fs> add /disk.img protocol:nbd server:tcp:example.com
 
 The possible I<-a URI> formats are described below.
 
@@ -1144,28 +1144,6 @@ The possible I<-a URI> formats are described below.
 
 Add the local disk image (or device) called F<disk.img>.
 
-=head2 B<-a ftp://[user@]example.com[:port]/disk.img>
-
-=head2 B<-a ftps://[user@]example.com[:port]/disk.img>
-
-=head2 B<-a http://[user@]example.com[:port]/disk.img>
-
-=head2 B<-a https://[user@]example.com[:port]/disk.img>
-
-Add a disk located on a remote FTP or HTTP server.
-
-The equivalent API command would be:
-
- ><fs> add /disk.img protocol:(ftp|...) server:tcp:example.com
-
-=head2 B<-a iscsi://example.com[:port]/target-iqn-name[/lun]>
-
-Add a disk located on an iSCSI server.
-
-The equivalent API command would be:
-
- ><fs> add target-iqn-name/lun protocol:iscsi server:tcp:example.com
-
 =head2 B<-a nbd://example.com[:port]>
 
 =head2 B<-a nbd://example.com[:port]/exportname>
@@ -1200,23 +1178,13 @@ The equivalent API command would be:
 
  ><fs> add pool/disk protocol:rbd server:tcp:example.com:port
 
-=head2 B<-a ssh://[user@]example.com[:port]/disk.img>
-
-Add a disk image located on a remote server, accessed using the Secure
-Shell (ssh) SFTP protocol.  SFTP is supported out of the box by all
-major SSH servers.
-
-The equivalent API command would be:
-
- ><fs> add /disk protocol:ssh server:tcp:example.com [username:user]
-
 Note that the URIs follow the syntax of
 L<RFC 3986|https://tools.ietf.org/html/rfc3986>: in particular, there
 are restrictions on the allowed characters for the various components
 of the URI.  Characters such as C<:>, C<@>, and C</> B<must> be
 percent-encoded:
 
- $ guestfish -a ssh://user:pass%40word@example.com/disk.img
+ $ guestfish -a rbd://user:pass%40word@example.com[:port]/pool/disk
 
 In this case, the password is C<pass@word>.
 
diff --git a/fish/test-add-uri.sh b/fish/test-add-uri.sh
index e4e1021db..8419ce78a 100755
--- a/fish/test-add-uri.sh
+++ b/fish/test-add-uri.sh
@@ -40,10 +40,6 @@ function fail ()
 $VG guestfish -x -a file://$abs_builddir/test-add-uri.img </dev/null >test-add-uri.out 2>&1
 grep -sq 'add_drive ".*/test-add-uri.img"' test-add-uri.out || fail
 
-# curl
-$VG guestfish -x -a ftp://user@example.com/disk.img </dev/null >test-add-uri.out 2>&1
-grep -sq 'add_drive "/disk.img" "protocol:ftp" "server:tcp:example.com" "username:user"' test-add-uri.out || fail
-
 # NBD
 $VG guestfish -x -a nbd://example.com </dev/null >test-add-uri.out 2>&1
 grep -sq 'add_drive "" "protocol:nbd" "server:tcp:example.com"' test-add-uri.out || fail
@@ -63,22 +59,5 @@ grep -sq 'add_drive "pool/disk" "protocol:rbd" "server:tcp:example.com:6789"' te
 $VG guestfish -x -a rbd:///pool/disk </dev/null >test-add-uri.out 2>&1
 grep -sq 'add_drive "pool/disk" "protocol:rbd"' test-add-uri.out || fail
 
-# ssh
-$VG guestfish -x -a ssh://example.com/disk.img </dev/null >test-add-uri.out 2>&1
-grep -sq 'add_drive "/disk.img" "protocol:ssh" "server:tcp:example.com"' test-add-uri.out || fail
-
-$VG guestfish -x -a ssh://user@example.com/disk.img </dev/null >test-add-uri.out 2>&1
-grep -sq 'add_drive "/disk.img" "protocol:ssh" "server:tcp:example.com" "username:user"' test-add-uri.out || fail
-
-$VG guestfish -x -a ssh://user@example.com:2000/disk.img </dev/null >test-add-uri.out 2>&1
-grep -sq 'add_drive "/disk.img" "protocol:ssh" "server:tcp:example.com:2000" "username:user"' test-add-uri.out || fail
-
-# iSCSI
-$VG guestfish -x -a iscsi://example.com/iqn.2015-12.com.libguestfs:test1/0 </dev/null >test-add-uri.out 2>&1
-grep -sq 'add_drive "iqn.2015-12.com.libguestfs:test1/0" "protocol:iscsi" "server:tcp:example.com"' test-add-uri.out || fail
-
-$VG guestfish -x -a iscsi://user:password@example.com/iqn.2015-12.com.libguestfs:test2/0 </dev/null >test-add-uri.out 2>&1
-grep -sq 'add_drive "iqn.2015-12.com.libguestfs:test2/0" "protocol:iscsi" "server:tcp:example.com" "username:user" "secret:password"' test-add-uri.out || fail
-
 rm test-add-uri.out
 rm test-add-uri.img
diff --git a/generator/actions_core.ml b/generator/actions_core.ml
index 5e8de563a..5e347ca85 100644
--- a/generator/actions_core.ml
+++ b/generator/actions_core.ml
@@ -350,22 +350,6 @@ F<filename> is interpreted as a local file or device.
 This is the default if the optional protocol parameter
 is omitted.
 
-=item C<protocol = \"ftp\"|\"ftps\"|\"http\"|\"https\">
-
-Connect to a remote FTP or HTTP server.
-The C<server> parameter must also be supplied - see below.
-
-See also: L<guestfs(3)/FTP AND HTTP>
-
-=item C<protocol = \"iscsi\">
-
-Connect to the iSCSI server.
-The C<server> parameter must also be supplied - see below.
-The C<username> parameter may be supplied.  See below.
-The C<secret> parameter may be supplied.  See below.
-
-See also: L<guestfs(3)/ISCSI>.
-
 =item C<protocol = \"nbd\">
 
 Connect to the Network Block Device server.
@@ -382,15 +366,6 @@ The C<secret> parameter may be supplied.  See below.
 
 See also: L<guestfs(3)/CEPH>.
 
-=item C<protocol = \"ssh\">
-
-Connect to the Secure Shell (ssh) server.
-
-The C<server> parameter must be supplied.
-The C<username> parameter may be supplied.  See below.
-
-See also: L<guestfs(3)/SSH>.
-
 =back
 
 =item C<server>
@@ -401,11 +376,8 @@ is a list of server(s).
  Protocol       Number of servers required
  --------       --------------------------
  file           List must be empty or param not used at all
- ftp|ftps|http|https  Exactly one
- iscsi          Exactly one
  nbd            Exactly one
  rbd            Zero or more
- ssh            Exactly one
 
 Each list element is a string specifying a server.  The string must be
 in one of the following formats:
@@ -421,10 +393,10 @@ for the protocol is used (see F</etc/services>).
 
 =item C<username>
 
-For the C<ftp>, C<ftps>, C<http>, C<https>, C<iscsi>, C<rbd> and C<ssh>
-protocols, this specifies the remote username.
+For the C<rbd>
+protocol, this specifies the remote username.
 
-If not given, then the local username is used for C<ssh>, and no authentication
+If not given, then no authentication
 is attempted for ceph.  But note this sometimes may give unexpected results, for
 example if using the libvirt backend and if the libvirt backend is configured to
 start the qemu appliance as a special user such as C<qemu.qemu>.  If in doubt,
diff --git a/lib/drives.c b/lib/drives.c
index c068b8ecb..6e4453ce5 100644
--- a/lib/drives.c
+++ b/lib/drives.c
@@ -166,34 +166,6 @@ create_drive_non_file (guestfs_h *g,
   return drv;
 }
 
-static struct drive *
-create_drive_curl (guestfs_h *g,
-                   const struct drive_create_data *data)
-{
-  if (data->nr_servers != 1) {
-    error (g, _("curl: you must specify exactly one server"));
-    return NULL;
-  }
-
-  if (data->servers[0].transport != drive_transport_none &&
-      data->servers[0].transport != drive_transport_tcp) {
-    error (g, _("curl: only tcp transport is supported"));
-    return NULL;
-  }
-
-  if (STREQ (data->exportname, "")) {
-    error (g, _("curl: pathname should not be an empty string"));
-    return NULL;
-  }
-
-  if (data->exportname[0] != '/') {
-    error (g, _("curl: pathname must begin with a '/'"));
-    return NULL;
-  }
-
-  return create_drive_non_file (g, data);
-}
-
 static int
 nbd_port (void)
 {
@@ -261,67 +233,6 @@ create_drive_rbd (guestfs_h *g,
   return create_drive_non_file (g, data);
 }
 
-static struct drive *
-create_drive_ssh (guestfs_h *g,
-                  const struct drive_create_data *data)
-{
-  if (data->nr_servers != 1) {
-    error (g, _("ssh: you must specify exactly one server"));
-    return NULL;
-  }
-
-  if (data->servers[0].transport != drive_transport_none &&
-      data->servers[0].transport != drive_transport_tcp) {
-    error (g, _("ssh: only tcp transport is supported"));
-    return NULL;
-  }
-
-  if (STREQ (data->exportname, "")) {
-    error (g, _("ssh: pathname should not be an empty string"));
-    return NULL;
-  }
-
-  if (data->exportname[0] != '/') {
-    error (g, _("ssh: pathname must begin with a '/'"));
-    return NULL;
-  }
-
-  if (data->username && STREQ (data->username, "")) {
-    error (g, _("ssh: username should not be an empty string"));
-    return NULL;
-  }
-
-  return create_drive_non_file (g, data);
-}
-
-static struct drive *
-create_drive_iscsi (guestfs_h *g,
-                    const struct drive_create_data *data)
-{
-  if (data->nr_servers != 1) {
-    error (g, _("iscsi: you must specify exactly one server"));
-    return NULL;
-  }
-
-  if (data->servers[0].transport != drive_transport_none &&
-      data->servers[0].transport != drive_transport_tcp) {
-    error (g, _("iscsi: only tcp transport is supported"));
-    return NULL;
-  }
-
-  if (STREQ (data->exportname, "")) {
-    error (g, _("iscsi: target name should not be an empty string"));
-    return NULL;
-  }
-
-  if (data->exportname[0] == '/') {
-    error (g, _("iscsi: target string must not begin with a '/'"));
-    return NULL;
-  }
-
-  return create_drive_non_file (g, data);
-}
-
 /**
  * Create the special F</dev/null> drive.
  *
@@ -768,26 +679,6 @@ guestfs_impl_add_drive_opts (guestfs_h *g, const char *filename,
       drv = create_drive_file (g, &data);
     }
   }
-  else if (STREQ (protocol, "ftp")) {
-    data.protocol = drive_protocol_ftp;
-    drv = create_drive_curl (g, &data);
-  }
-  else if (STREQ (protocol, "ftps")) {
-    data.protocol = drive_protocol_ftps;
-    drv = create_drive_curl (g, &data);
-  }
-  else if (STREQ (protocol, "http")) {
-    data.protocol = drive_protocol_http;
-    drv = create_drive_curl (g, &data);
-  }
-  else if (STREQ (protocol, "https")) {
-    data.protocol = drive_protocol_https;
-    drv = create_drive_curl (g, &data);
-  }
-  else if (STREQ (protocol, "iscsi")) {
-    data.protocol = drive_protocol_iscsi;
-    drv = create_drive_iscsi (g, &data);
-  }
   else if (STREQ (protocol, "nbd")) {
     data.protocol = drive_protocol_nbd;
     drv = create_drive_nbd (g, &data);
@@ -796,10 +687,6 @@ guestfs_impl_add_drive_opts (guestfs_h *g, const char *filename,
     data.protocol = drive_protocol_rbd;
     drv = create_drive_rbd (g, &data);
   }
-  else if (STREQ (protocol, "ssh")) {
-    data.protocol = drive_protocol_ssh;
-    drv = create_drive_ssh (g, &data);
-  }
   else {
     error (g, _("unknown protocol ‘%s’"), protocol);
     drv = NULL; /*FALLTHROUGH*/
diff --git a/lib/guestfs.pod b/lib/guestfs.pod
index d2c90418e..85d765aa8 100644
--- a/lib/guestfs.pod
+++ b/lib/guestfs.pod
@@ -723,51 +723,6 @@ a qcow2 backing file specification, libvirt does not construct an
 ephemeral secret object from those, for Ceph authentication.  Refer to
 L<https://bugzilla.redhat.com/2033247>.
 
-=head3 FTP AND HTTP
-
-Libguestfs can access remote disks over FTP, FTPS, HTTP or HTTPS
-protocols.
-
-To do this, set the optional C<protocol> and C<server> parameters of
-L</guestfs_add_drive_opts> like this:
-
- char **servers = { "www.example.org", NULL };
- guestfs_add_drive_opts (g, "/disk.img",
-                         GUESTFS_ADD_DRIVE_OPTS_FORMAT, "raw",
-                         GUESTFS_ADD_DRIVE_OPTS_PROTOCOL, "http",
-                         GUESTFS_ADD_DRIVE_OPTS_SERVER, servers,
-                         -1);
-
-The C<protocol> can be one of C<"ftp">, C<"ftps">, C<"http">,
-or C<"https">.
-
-C<servers> (the C<server> parameter) is a list which must have a
-single element.  The single element is a string defining the web
-or FTP server.  The format of this string is documented in
-L</guestfs_add_drive_opts>.
-
-=head3 GLUSTER
-
-Glusterfs support was removed in libguestfs 1.54 (2024).
-
-=head3 ISCSI
-
-Libguestfs can access iSCSI disks remotely.
-
-To do this, set the optional C<protocol> and C<server> parameters like
-this:
-
- char **server = { "iscsi.example.org:3000", NULL };
- guestfs_add_drive_opts (g, "target-iqn-name/lun",
-                         GUESTFS_ADD_DRIVE_OPTS_FORMAT, "raw",
-                         GUESTFS_ADD_DRIVE_OPTS_PROTOCOL, "iscsi",
-                         GUESTFS_ADD_DRIVE_OPTS_SERVER, server,
-                         -1);
-
-The C<server> parameter is a list which must have a single element.
-The single element is a string defining the iSCSI server.  The format
-of this string is documented in L</guestfs_add_drive_opts>.
-
 =head3 NETWORK BLOCK DEVICE
 
 Libguestfs can access Network Block Device (NBD) disks remotely.
@@ -830,28 +785,6 @@ L<https://bugs.launchpad.net/qemu/+bug/1155677>
 
 =back
 
-=head3 SHEEPDOG
-
-Sheepdog support was removed in libguestfs 1.54 (2024).
-
-=head3 SSH
-
-Libguestfs can access disks over a Secure Shell (SSH) connection.
-
-To do this, set the C<protocol> and C<server> and (optionally)
-C<username> parameters of L</guestfs_add_drive_opts> like this:
-
- char **server = { "remote.example.com", NULL };
- guestfs_add_drive_opts (g, "/path/to/disk.img",
-                         GUESTFS_ADD_DRIVE_OPTS_FORMAT, "raw",
-                         GUESTFS_ADD_DRIVE_OPTS_PROTOCOL, "ssh",
-                         GUESTFS_ADD_DRIVE_OPTS_SERVER, server,
-                         GUESTFS_ADD_DRIVE_OPTS_USERNAME, "remoteuser",
-                         -1);
-
-The format of the server string is documented in
-L</guestfs_add_drive_opts>.
-
 =head2 INSPECTION
 
 Libguestfs has APIs for inspecting an unknown disk image to find out
diff --git a/tests/disks/test-qemu-drive-libvirt.sh b/tests/disks/test-qemu-drive-libvirt.sh
index 485d75718..e917cd1a6 100755
--- a/tests/disks/test-qemu-drive-libvirt.sh
+++ b/tests/disks/test-qemu-drive-libvirt.sh
@@ -65,13 +65,6 @@ check_output
 grep -sq -- '-drive file=rbd:abc-def/ghi-jkl:auth_supported=none,' "$DEBUG_QEMU_FILE" || fail ceph2
 rm "$DEBUG_QEMU_FILE"
 
-# iSCSI.
-
-$guestfish -d iscsi run ||:
-check_output
-grep -sq -- '-drive file=iscsi://1.2.3.4:1234/iqn.2003-01.org.linux-iscsi.fedora' "$DEBUG_QEMU_FILE" || fail iscsi
-rm "$DEBUG_QEMU_FILE"
-
 # NBD.
 
 $guestfish -d nbd run ||:
diff --git a/tests/disks/test-qemu-drive.sh b/tests/disks/test-qemu-drive.sh
index d6ce0f07f..153b9c8cc 100755
--- a/tests/disks/test-qemu-drive.sh
+++ b/tests/disks/test-qemu-drive.sh
@@ -63,35 +63,6 @@ check_output
 grep -sq -- '-drive file=rbd:abc-def/ghi-jkl:auth_supported=none,' "$DEBUG_QEMU_FILE" || fail
 rm "$DEBUG_QEMU_FILE"
 
-# HTTP.
-
-guestfish <<EOF ||:
-  add "/disk.img" "format:raw" "protocol:http" "server:www.example.com"
-  run
-EOF
-check_output
-grep -sq -- '-drive file=http://www.example.com/disk.img,' "$DEBUG_QEMU_FILE" || fail
-rm "$DEBUG_QEMU_FILE"
-
-# iSCSI.
-
-guestfish <<EOF ||:
-  add "target-iqn-name/lun" "format:raw" "protocol:iscsi" "server:www.example.com:3000"
-  run
-EOF
-check_output
-grep -sq -- '-drive file=iscsi://www.example.com:3000/target-iqn-name/lun,' "$DEBUG_QEMU_FILE" || fail
-rm "$DEBUG_QEMU_FILE"
-
-guestfish <<EOF ||:
-  add "target-iqn-name/lun" "format:raw" "protocol:iscsi" "server:www.example.com:3000" \
-    "username:user" "secret:pass"
-  run
-EOF
-check_output
-grep -sq -- '-drive file=iscsi://user%pass@www.example.com:3000/target-iqn-name/lun,' "$DEBUG_QEMU_FILE" || fail
-rm "$DEBUG_QEMU_FILE"
-
 # NBD.
 
 guestfish <<EOF ||:
@@ -109,14 +80,3 @@ EOF
 check_output
 grep -sq -- '-drive file=nbd:unix:/socket,' "$DEBUG_QEMU_FILE" || fail
 rm "$DEBUG_QEMU_FILE"
-
-# SSH.
-
-guestfish <<EOF ||:
-  add "/disk.img" "format:raw" "protocol:ssh" "server:example.com" \
-    "username:rich"
-  run
-EOF
-check_output
-grep -sq -- '-drive file=ssh://rich@example.com/disk.img,' "$DEBUG_QEMU_FILE" || fail
-rm "$DEBUG_QEMU_FILE"
-- 
2.47.1

